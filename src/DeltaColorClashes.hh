#ifndef _DELTACOLORCLASHES_HH
#define _DELTACOLORCLASHES_HH

#include "Graph.hh"
#include "Coloring.hh"
#include "Recolor.hh"
#include "ColorClashes.hh"

#include <helpers/DeltaCostComponent.hh>

class TabledDeltaColorClashes
: public DeltaCostComponent<Graph, Coloring, Recolor, int>
{
public:
  TabledDeltaColorClashes(const Graph& in, ColorClashes& cc)
  : DeltaCostComponent<Graph, Coloring, Recolor, int>(in, cc, "TabledColorClashes")
{}
  int ComputeDeltaCost(const Coloring& st, const Recolor& rc) const;
};

class DeltaColorClashes
: public DeltaCostComponent<Graph, Coloring, Recolor, int>
{
public:
  DeltaColorClashes(const Graph& in, ColorClashes& cc)
  : DeltaCostComponent<Graph, Coloring, Recolor, int>(in, cc, "DeltaColorClashes")
  {}
  int ComputeDeltaCost(const Coloring& st, const Recolor& rc) const;
};

class AutoDeltaColorClashes
: public DeltaCostComponent<Graph, AutoColoring, Recolor, int>
{
public:
  AutoDeltaColorClashes(const Graph& in, AutoColorClashes& cc)
  : DeltaCostComponent<Graph, AutoColoring, Recolor, int>(in, cc, "AutoDeltaColorClashes")
  {}
  int ComputeDeltaCost(const AutoColoring& st, const Recolor& rc) const;
};


#endif
