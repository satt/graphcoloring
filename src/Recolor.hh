#ifndef _RECOLOR_HH
#define _RECOLOR_HH

// Move class

#include "Graph.hh"
#include <iostream>

class Recolor
{
  friend std::ostream& operator<<(std::ostream&, const Recolor&);
  friend std::istream& operator>>(std::istream&, Recolor&);
public:
    Vertex v;   // the node
  Color nc;      // the new color assigned to the node
  Color oc;      // the old color
  bool operator==(const Recolor&) const;
  bool operator!=(const Recolor& mv) const
  { return !(*this == mv); }
  bool operator<(const Recolor&) const;
};

#endif
