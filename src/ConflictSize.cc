#include "ConflictSize.hh"

int ConflictSize::ComputeCost(const Coloring& coloring) const
{
  return coloring.ConflictsSize();
}
