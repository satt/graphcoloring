#include "DeltaColorClashes.hh"

int TabledDeltaColorClashes::ComputeDeltaCost(const Coloring& coloring, const Recolor& rc) const
{
  int current = 0;
  for (Vertex w = 0; w < in.Vertices(); w++)
    if (in.Edge(rc.v, w))
    {
      if (coloring[w] == rc.oc)
        current--;
      if (coloring[w] == rc.nc)
        current++;
    }
  return current;
  //return coloring.adjacent_conflicts[rc.v][rc.nc] - coloring.adjacent_conflicts[rc.v][rc.oc];/
}

int DeltaColorClashes::ComputeDeltaCost(const Coloring& coloring, const Recolor& rc) const
{
  int delta = 0;
  for (Vertex w = 0; w < in.Vertices(); w++)
    if (in.Edge(rc.v, w))
    {
      if (rc.oc == coloring[w])
        delta--;
      if (rc.nc == coloring[w])
        delta++;
    }
  
  return delta;
}

int AutoDeltaColorClashes::ComputeDeltaCost(const AutoColoring& coloring, const Recolor& rc) const
{
  coloring.simulate(coloring.color[rc.v] <<= (int)rc.nc, 1);
  return coloring.value_of(coloring.cost, 1) - coloring.value_of(coloring.cost, 0);
}
