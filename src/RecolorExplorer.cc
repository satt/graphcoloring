#include "RecolorExplorer.hh"

#include <utils/Random.hh>
#include <sstream>
#include <stdexcept>

RecolorExplorer::RecolorExplorer(const Graph& in, StateManager<Graph,Coloring,int>& sm)
: NeighborhoodExplorer<Graph,Coloring,Recolor,int>(in, sm, "RecolorExplorer")
{}

// random move builder
void RecolorExplorer::RandomMove(const Coloring& coloring, Recolor& rc) const throw (EmptyNeighborhood)
{
  if (coloring.ConflictsSize() == 0)
    throw EmptyNeighborhood();
  int i = Random::Int(0, coloring.conflicting_nodes.size() - 1);
  for (Vertex v : coloring.conflicting_nodes)
  {
    rc.v = v;
    if (i == 0)
      break;
    i--;
  }
  rc.oc = coloring[rc.v];
  do
    rc.nc = (Color)Random::Int(0, in.k - 1);
  while (rc.nc == rc.oc);
}

// random move builder
void RecolorExplorer::FirstMove(const Coloring& coloring, Recolor& rc) const throw (EmptyNeighborhood)
{
  if (coloring.ConflictsSize() == 0)
    throw EmptyNeighborhood();
  rc.v = *coloring.conflicting_nodes.begin();
  rc.oc = coloring[rc.v];
  rc.nc = 0;
  while (rc.nc == rc.oc)
    rc.nc++;
}


// update the state according to the move
void RecolorExplorer::MakeMove(Coloring& coloring, const Recolor& rc) const
{
//  static int i = 0;
  // for the identical move
  if (rc.nc == rc.oc)
    return; // nothing has to be done
  // else make the actual move
  coloring[rc.v] = rc.nc;
  for (Vertex w = 0; w < in.Vertices(); w++)
    if (in.Edge(rc.v, w))
    {
      if (coloring[w] == rc.oc)
      {
        coloring.adjacent_conflicts[w]--;
        coloring.adjacent_conflicts[rc.v]--;
      }
      if (coloring.adjacent_conflicts[w] == 0)
        coloring.conflicting_nodes.erase(w);
      if (coloring[w] == rc.nc)
      {
        coloring.adjacent_conflicts[w]++;
        coloring.adjacent_conflicts[rc.v]++;
        coloring.conflicting_nodes.insert(w);
      }
    }
  if (coloring.adjacent_conflicts[rc.v] == 0)
    coloring.conflicting_nodes.erase(rc.v);
  else
    coloring.conflicting_nodes.insert(rc.v);

//  std::cerr << "D++++";
//  std::for_each(coloring.conflicting_nodes.begin(), coloring.conflicting_nodes.end(), [](const int& v) { std::cerr << v << " "; });
//  std::cerr << std::endl;
//  std::cerr << "D----";
//  for (Vertex v = 0; v < in.Vertices(); v++)
//  {
//    bool in_conflict = false;
//    for (Vertex w = 0; w < in.Vertices(); w++)
//      if (in.Edge(v, w) && coloring[v] == coloring[w])
//      {
//        in_conflict = true;
//        break;
//      }
//    if (in_conflict)
//      std::cerr << v << " ";
//  }
//  std::cerr << std::endl;
}

// compute the next move in the exploration of the neighborhood
bool RecolorExplorer::NextMove(const Coloring& coloring, Recolor& rc) const
{
  rc.nc = (rc.nc + 1) % in.k;
  // try the next conflicting node skipping the trivial move (i.e. nc = oc)
  if (rc.nc == rc.oc)
  {
    do
      rc.v = rc.v + 1;
    while (rc.v < in.Vertices() && !coloring.ConflictingNode(rc.v));
    if (rc.v >= in.Vertices())
      return false;
    rc.oc = coloring[rc.v];
    rc.nc = (rc.oc + 1) % in.k;
  }
  return true;
}

AutoRecolorExplorer::AutoRecolorExplorer(const Graph& in, StateManager<Graph,AutoColoring,int>& sm)
: NeighborhoodExplorer<Graph,AutoColoring,Recolor,int>(in, sm, "AutoRecolorExplorer")
{}

// random move builder
void AutoRecolorExplorer::RandomMove(const AutoColoring& coloring, Recolor& rc) const throw (EmptyNeighborhood)
{
  if (coloring.CostValue() == 0)
    throw EmptyNeighborhood();
  int i = Random::Int(0, coloring.conflicting_nodes.size() - 1);
  for (Vertex v : coloring.conflicting_nodes)
  {
    rc.v = v;
    if (i == 0)
      break;
    i--;
  }
  rc.oc = coloring[rc.v];
  do
    rc.nc = (Color)Random::Int(0, in.k - 1);
  while (rc.nc == rc.oc);
}

// random move builder
void AutoRecolorExplorer::FirstMove(const AutoColoring& coloring, Recolor& rc) const throw (EmptyNeighborhood)
{
  if (coloring.CostValue() == 0)
    throw EmptyNeighborhood();
  rc.v = *coloring.conflicting_nodes.begin();
  rc.oc = coloring[rc.v];
  rc.nc = 0;
  while (rc.nc == rc.oc)
    rc.nc++;
}


// update the state according to the move
void AutoRecolorExplorer::MakeMove(AutoColoring& coloring, const Recolor& rc) const
{
//  static int i = 0;
  // for the identical move
  if (rc.nc == rc.oc)
    return; // nothing has to be done
  // else make the actual move
  coloring.execute(coloring.color[rc.v] <<= (int)rc.nc);
  for (Vertex w = 0; w < in.Vertices(); w++)
    if (in.Edge(rc.v, w))
    {
      if (coloring.AdjacentConflicts(w) == 0)
        coloring.conflicting_nodes.erase(w);
      else 
        coloring.conflicting_nodes.insert(w);
    }
  if (coloring.AdjacentConflicts(rc.v) == 0)
    coloring.conflicting_nodes.erase(rc.v);
  else
    coloring.conflicting_nodes.insert(rc.v);
//  std::cerr << "A++++";
//  std::for_each(coloring.conflicting_nodes.begin(), coloring.conflicting_nodes.end(), [](const int& v) { std::cerr << v << " "; });
//  std::cerr << std::endl;
//  std::cerr << "A----";
//  for (Vertex v = 0; v < in.Vertices(); v++)
//  {
//    bool in_conflict = false;
//    for (Vertex w = 0; w < in.Vertices(); w++)
//      if (in.Edge(v, w) && coloring.value_of(coloring.color[v]) == coloring.value_of(coloring.color[w]))
//      {
//        in_conflict = true;
//        break;
//      }
//    if (in_conflict)
//      std::cerr << v << " ";
//  }
//  std::cerr << std::endl;
//  i++;
}

// compute the next move in the exploration of the neighborhood
bool AutoRecolorExplorer::NextMove(const AutoColoring& coloring, Recolor& rc) const
{
  rc.nc = (rc.nc + 1) % in.k;
  // try the next conflicting node skipping the trivial move (i.e. nc = oc)
  if (rc.nc == rc.oc)
  {
    do
      rc.v = rc.v + 1;
    while (rc.v < in.Vertices() && !coloring.ConflictingNode(rc.v));
    if (rc.v >= in.Vertices())
      return false;
    rc.oc = coloring[rc.v];
    rc.nc = (rc.oc + 1) % in.k;
  }
  return true;
}
