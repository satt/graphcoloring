#include "LargestDegreeColoringManager.hh"

#include <queue>
#include <utils/Random.hh>

LargestDegreeColoringManager::LargestDegreeColoringManager(const Graph& in)
: StateManager<Graph, Coloring>(in)
{}

// initial state builder
void LargestDegreeColoringManager::RandomState(Coloring& coloring)
{
  std::vector<std::pair<Vertex,int> > degree(in.Vertices());
  std::queue<Vertex> degree_queue;
  
  for (Vertex v = 0; v < in.Vertices(); v++)
  {
    coloring[v] = in.k;
    degree[v].first = v;
    degree[v].second = 0;
  }
  for (Vertex v = 0; v < in.Vertices(); v++)
    for (Vertex w = 0; w < in.Vertices(); w++)
      if (in.Edge(v,w))
        degree[v].second++;
  
  for (Vertex v = 0; v < in.Vertices(); v++)
  {
    Vertex max_degree_vertex = degree[v].first;
    assert(degree[v].first == v);
    for (Vertex w = 0; w < in.Vertices(); w++)
      if (degree[w].second > degree[max_degree_vertex].second 
					|| (degree[w].second == degree[max_degree_vertex].second && Random::Int(0,1) == 1))
        max_degree_vertex = degree[w].first;
    degree_queue.push(max_degree_vertex);
    degree[max_degree_vertex].second = -1;
  }
  
  while (!degree_queue.empty())
  {
    Vertex v = degree_queue.front();
    degree_queue.pop();
    assert(coloring[v] == in.k);
    int min_cost = in.Vertices();
    Color min_c = 0;
    for (Color c = 0; c < in.k; c++)
    {
      int cost_c = 0;
      for (Vertex w = 0; w < in.Vertices(); w++)
        if (in.Edge(v,w))
          if (c == coloring[w])
            cost_c++;
      if (cost_c < min_cost || (cost_c == min_cost && Random::Int(0,1) == 1))
      {
        min_cost = cost_c;
        min_c = c;
      }
    }
    coloring[v] = min_c;
  }
  
  coloring.UpdateRedundantData();
}
