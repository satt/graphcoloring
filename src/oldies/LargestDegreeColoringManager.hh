#ifndef _LARGESTDEGREECOLORINGMANAGER_HH
#define _LARGESTDEGREECOLORINGMANAGER_HH

#include "ColoringManager.hh"

// State Manager

class LargestDegreeColoringManager
: public StateManager<Graph, Coloring>
{
public:
  LargestDegreeColoringManager(const Graph&);
  void RandomState(Coloring&);
};

#endif
