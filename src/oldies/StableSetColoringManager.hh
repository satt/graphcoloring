#ifndef _STABLESETCOLORINGMANAGER_HH
#define _STABLESETCOLORINGMANAGER_HH

#include "Graph.hh"
#include "Coloring.hh"
#include <helpers/StateManager.hh>

class StableSetColoringManager
: public StateManager<Graph, Coloring, int>
{
public:
  StableSetColoringManager(const Graph&);
  void RandomState(Coloring&);
};

#endif
