#include "StableSetColoringManager.hh"

#include "Graph.hh"
#include <utils/Random.hh>

// constructor
StableSetColoringManager::StableSetColoringManager(const Graph& in)
: StateManager<Graph,Coloring, int>(in, "StableSetColoringManager")
{}

// initial state builder
void StableSetColoringManager::RandomState(Coloring& coloring)
{
  Color c = 0;
  VertexSet H1, H2, stable_set;
  VertexSet::iterator hv, hend;
  std::vector<Color> p(in.k);
  
  for (Color col = 0; col < in.k; col++)
  {
    p[col] = col;
  }
  
  // randomly permute colors
  
  for (Vertex v = 0; v < in.Vertices(); v++)
    H1.insert(v);
  
  while (!H1.empty() && c < in.k)
  {
    H2 = H1;
    stable_set.clear();
    for (hv = H2.begin(), hend = H2.end(); hv != hend; hv++)
    {
      stable_set.insert(*hv);
      for (Vertex w = 0; w < in.Vertices(); w++)
        if (in.Edge(*hv,w))
          H2.erase(w);
    }
    // assign the color to the currently found stable set
    for (hv = stable_set.begin(), hend = stable_set.end(); hv != hend; hv++)
    {
      H1.erase(*hv);
      coloring[*hv] = p[c];
    }
    c++; // go to the next color class
  }
  // now assign all remaining node randomly
  for (hv = H1.begin(), hend=H1.end(); hv != hend; hv++)
    coloring[*hv] = Random::Int(0, in.k - 1);
  
  coloring.UpdateRedundantData();
}
