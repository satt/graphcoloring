#ifndef _COLORINGMANAGER_HH
#define _COLORINGMANAGER_HH

#include "Graph.hh"
#include "Coloring.hh"
#include <helpers/StateManager.hh>

class ColoringManager
: public StateManager<Graph, Coloring, int>
{
public:
  ColoringManager(const Graph&);
  void RandomState(Coloring&);
  bool CheckConsistency(const Coloring& st) const
  { return true; }
};

class AutoColoringManager
: public StateManager<Graph, AutoColoring, int>
{
public:
  AutoColoringManager(const Graph&);
  void RandomState(AutoColoring&);
  bool CheckConsistency(const AutoColoring& st) const
  { return true; }
};

#endif
