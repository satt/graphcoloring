

#ifndef _GRAPH_HH
#define _GRAPH_HH

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

#include <vector>
#include <set>

typedef int Color;
typedef int Vertex;

// Input class

class Graph

{
  friend std::ostream& operator<<(std::ostream&, const Graph&);
public:
  Graph() {}
  Graph(const std::string& instance_desc);
  bool Edge(const Vertex& v, const Vertex& w) const
  {
    return adjacency_matrix[v][w];
  }
  int Vertices() const
  {
    return num_of_vertices;
  }
	int Edges() const
	{
		return num_of_edges;
	}
  void Load(const std::string& filename, int ncols);
	int k;
protected:
  int num_of_vertices;
  int num_of_edges;
  std::vector<std::vector<bool> > adjacency_matrix;
};

#endif
