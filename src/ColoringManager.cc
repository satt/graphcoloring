#include "ColoringManager.hh"

#include <utils/Random.hh>

// constructor
ColoringManager::ColoringManager(const Graph& in)
: StateManager<Graph,Coloring,int>(in, "ColoringManager")
{}

// initial state builder
void ColoringManager::RandomState(Coloring& coloring)
{
  auto start = std::chrono::system_clock::now();
   // assign all nodes randomly
  for (Vertex v = 0; v < in.Vertices(); v++)
    coloring[v] = Random::Int(0, in.k - 1);
  
  coloring.UpdateRedundantData();
  std::cout << "Random state: " <<  std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start).count() / 1000000.0 << std::endl;
}

// constructor
AutoColoringManager::AutoColoringManager(const Graph& in)
: StateManager<Graph, AutoColoring, int>(in, "AutoColoringManager")
{}

// initial state builder
void AutoColoringManager::RandomState(AutoColoring& coloring)
{
  auto start = std::chrono::system_clock::now();
  // assign all nodes randomly
  for (Vertex v = 0; v < in.Vertices(); v++)
    coloring.set(coloring.color[v], Random::Int(0, in.k - 1));

  std::cout << "Random state before evaluate: " <<  std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() / 1000.0 << std::endl;
  coloring.evaluate();
  coloring.UpdateRedundantData();
  std::cout << "Random state: " <<  std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() / 1000.0 << std::endl;
}
