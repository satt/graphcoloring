#ifndef _COLORCLASHES_HH
#define _COLORCLASHES_HH

#include "Graph.hh"
#include "Coloring.hh"

#include <helpers/CostComponent.hh>

class ColorClashes
: public CostComponent<Graph, Coloring, int>
{
public:
  ColorClashes(const Graph& in)
  : CostComponent<Graph, Coloring, int>(in, 1, true, "Color Clashes")
  {}
int ComputeCost(const Coloring& st) const;
  void PrintViolations(const Coloring& st, std::ostream& os = std::cout) const {}
};

class AutoColorClashes
: public CostComponent<Graph, AutoColoring, int>
{
public:
  AutoColorClashes(const Graph& in)
  : CostComponent<Graph, AutoColoring, int>(in, 1, true, "Auto Color Clashes")
  {}
  int ComputeCost(const AutoColoring& st) const;
  void PrintViolations(const AutoColoring& st, std::ostream& os = std::cout) const {}
};

#endif
