#ifndef _RECOLOREXPLORER_HH
#define _RECOLOREXPLORER_HH

#include "Graph.hh"
#include "Coloring.hh"
#include "Recolor.hh"
#include <helpers/NeighborhoodExplorer.hh>

class RecolorExplorer
: public NeighborhoodExplorer<Graph, Coloring, Recolor, int>
{
public:
  RecolorExplorer(const Graph& in, StateManager<Graph, Coloring, int>&);
  void RandomMove(const Coloring&, Recolor&) const throw (EmptyNeighborhood);
  void FirstMove(const Coloring&, Recolor&) const throw (EmptyNeighborhood);
  void MakeMove(Coloring&, const Recolor&) const;
protected:
  bool NextMove(const Coloring&, Recolor&) const;
};

class AutoRecolorExplorer
: public NeighborhoodExplorer<Graph, AutoColoring, Recolor, int>
{
public:
  AutoRecolorExplorer(const Graph& in, StateManager<Graph, AutoColoring, int>&);
  void RandomMove(const AutoColoring&, Recolor&) const throw (EmptyNeighborhood);
  void FirstMove(const AutoColoring&, Recolor&) const throw (EmptyNeighborhood);
  void MakeMove(AutoColoring&, const Recolor&) const;
protected:
  bool NextMove(const AutoColoring&, Recolor&) const;
};

#endif
