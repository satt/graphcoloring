#include "Graph.hh"

#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <vector>

void StringSplit(std::string str, std::string delim, std::vector<std::string>& results)
{
	int cutAt;
	while( (cutAt = str.find_first_of(delim)) != str.npos )
	{
		if(cutAt > 0)
			results.push_back(str.substr(0,cutAt));
		str = str.substr(cutAt+1);
	}
	if(str.length() > 0)
		results.push_back(str);
}

Graph::Graph(const std::string& instance_desc)
{
	std::string filename;
	std::vector<std::string> res;
	StringSplit(instance_desc, ":", res);
	Load(res[0], atoi(res[1].c_str()));
}

void Graph::Load(const std::string& filename, Color ncols)
{
	k = ncols;
  std::ifstream is(filename.c_str());
  if (!is)
    throw std::runtime_error("Could not open file " + filename);
	std::ostringstream oss;
  char buf[255], *tmp; // line buffer
  int nn, en = 0;
  int cen = 0; // for doublechecking file definition (counts the read edges)
	int line = 0;
  
  while (!is.eof())
  {
    is.getline(buf, 255); // process each line
		line++;
    switch (buf[0])
    {
      case 'p':
      {
        // reads the number of nodes and creates them
        tmp = strtok(buf, "edge"); // splits buf around the 'edge' token
        tmp = strtok(0, "edge"); // and considers the second substring
        tmp = strtok(tmp, " "); // now process for the first value
        nn = atoi(tmp);
        tmp = strtok(0, " "); // and for the second one
        en = atoi(tmp);
        // let's ignore everything else
        // create the matrix
        num_of_vertices = nn;
        adjacency_matrix.resize(num_of_vertices, std::vector<bool>(num_of_vertices, 0));
        /* adjacency_matrix = new bool *[num_of_vertices];
        for (Vertex v = 0; v < num_of_vertices; v++)
        {
          adjacency_matrix[v] = new unsigned char[num_of_vertices];
          for (Vertex w = 0; w < num_of_vertices; w++)
            adjacency_matrix[v][w] = false;
        } */
      }
        break;
      case 'e':
      {
        // reads the edge information and add the edge between the
        // corresponding nodes
        tmp = strtok(buf, " "); // splits buf among the spaces
        tmp = strtok(0, " "); // consider the first number
        Vertex v = atoi(tmp) - 1;
        tmp = strtok(0, " "); // and now the second one
        Vertex w = atoi(tmp) - 1;
        adjacency_matrix[v][w] = adjacency_matrix[w][v] = true;
        cen++;
      }
        break;
      case 'c':
      case ' ':
      case '\0':
        // ignore the comments and the empty lines
        break;
      default:
				oss << "Error while reading the input file " << filename << " at line " << line << " the content was not understandable";
				throw new std::runtime_error(oss.str());
    }
  }
  num_of_edges = cen;
}

std::ostream& operator<<(std::ostream& os, const Graph& g)
{
  os << "c *********************************************************" << std::endl
  << "c Output file from EasyLocal++ GraphColoring implementation" << std::endl
  << "c *********************************************************" << std::endl
  << "p edge " << g.num_of_vertices << " " << g.num_of_edges << std::endl;
  
  for (Vertex v = 0; v < g.Vertices(); v++)
    for (Vertex w = v + 1; w < g.Vertices(); w++)
      if (g.Edge(v,w))
        os << "e " << w << " " << v << std::endl;
  return os;
}
