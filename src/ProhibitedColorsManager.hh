#ifndef _PROHIBITEDCOLORSMANAGER_HH
#define _PROHIBITEDCOLORSMANAGER_HH

#include "Graph.hh"
#include "Coloring.hh"
#include "Recolor.hh"

#include <helpers/TabuListManager.hh>

class ClassicalTabuListManager
: public TabuListManager<Coloring, Recolor, int>
{
public:
  ClassicalTabuListManager(int min = 0, int max = 0, std::string name="Classical Tabu List Manager");
protected:
  bool Inverse(const Recolor&,const Recolor&) const;
};

class ProhibitedColorsManager
: public ClassicalTabuListManager
{
public:
  ProhibitedColorsManager(const Parameter<float>& alpha, int min = 0, int max = 0);
protected:
  void InsertIntoList(const Coloring& coloring, const Recolor& rc);
  const Parameter<float>& alpha;
};

#endif
