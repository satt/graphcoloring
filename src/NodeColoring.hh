#ifndef _NODECOLORING_HH
#define _NODECOLORING_HH

#include <iostream>
#include "Graph.hh"

// Output class

class NodeColoring : public std::vector<Color>
{
  friend std::ostream& operator<<(std::ostream&, const NodeColoring&);
  friend std::istream& operator>>(std::istream&, NodeColoring&);
public:
  using std::vector<Color>::operator[];
  NodeColoring(const Graph& in) : g(in)
  {
    resize(g.Vertices());
  }
  NodeColoring& operator=(const NodeColoring& col)
  {
    static_cast<std::vector<Color>& >(*this) = static_cast<const std::vector<Color>& >(col);
    return *this;
  }
  const Graph& g;
};

#endif
