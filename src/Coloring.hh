#ifndef _COLORING_HH
#define _COLORING_HH

#include "Graph.hh"
#include "NodeColoring.hh"
#include <iostream>
#include <vector>
#include <set>
#include "autostate.hh"

// State class

using namespace easylocal::modeling;

class Coloring : public NodeColoring
{
  friend class RecolorExplorer;
public:
  Coloring(const Graph& in);  
  void UpdateRedundantData();
  bool ConflictingNode(Vertex v) const
  {
    return adjacent_conflicts[v] > 0;
  }
  int ConflictsSize() const
  {
    return conflicting_nodes.size();
  }
  std::vector<int> adjacent_conflicts;
  std::set<int> conflicting_nodes;
protected:
};

class AutoColoring : public AutoState<int>
{
  friend class AutoColoringManager;
  friend class AutoRecolorExplorer;
  friend class AutoDeltaColorClashes;
  friend class AutoNodeColoringManager;
  friend bool operator==(const AutoColoring& st1, const AutoColoring& st2);
public:
  AutoColoring(const Graph& in);
  int CostValue() const
  {
    return this->st(cost);
  }
  const int& operator[](int i) const
  {
    return this->st(color[i]);
  }
  bool ConflictingNode(Vertex v) const
  {
    return this->st(adjacent_conflicts[v]) > 0;
  }
  int AdjacentConflicts(Vertex v) const
  {
    return this->st(adjacent_conflicts[v]);
  }
  virtual void print(std::ostream& os) const
  {
    for (size_t i = 0; i < color.size(); i++)
      os << value_of(color[i]) << " ";
    os << std::endl << "Conflicting edges: " << value_of(cost) << ", Conflicting nodes: " << conflicting_nodes.size();
  }
  void UpdateRedundantData();
  //  AutoColoring& operator=(const AutoColoring& st);
protected:
  const Graph* in;
  /** Color of nodes */
  VarArray<int> color;
  /** Value of hard constraints */
  CompiledExpression<int> cost;
  std::vector<CompiledExpression<int>> adjacent_conflicts;
  std::set<int> conflicting_nodes;
};

bool operator==(const AutoColoring& st1, const AutoColoring& st2);

#endif
