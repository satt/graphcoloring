#include "Graph.hh"
#include "NodeColoring.hh"
#include "Coloring.hh"
#include "Recolor.hh"
#include "ColoringManager.hh"
#include "NodeColoringManager.hh"
#include "RecolorExplorer.hh"
#include "ColorClashes.hh"
#include "DeltaColorClashes.hh"
#include "ProhibitedColorsManager.hh"
#include <Runners.hh>
#include <solvers/SimpleLocalSearch.hh>
#include <Testers.hh>
#include <string>
#include <fstream>

int run_direct_modeling(int argc, const char* argv[]);
int run_auto_modeling(int argc, const char* argv[]);

int main(int argc, const char* argv[])
{
  try
  {
    std::vector<std::string> arguments(argv + 1, argv + argc);
    if (std::any_of(arguments.begin(), arguments.end(), [](const std::string& s) { return s == "auto"; }))
      return run_auto_modeling(argc, argv);
    else
      return run_direct_modeling(argc, argv);
  }
  catch (std::logic_error e)
  {
    std::cerr << "Exception: " << e.what() << std::endl;
    return 1;
  }
  
	return 0;
}

int run_direct_modeling(int argc, const char* argv[])
{
  auto start = std::chrono::system_clock::now();
  ParameterBox main_parameters("main", "Main Program options");
  /** Input parameters **/
  Parameter<std::string> input_file("i", "input file", main_parameters);
  Parameter<unsigned int> number_of_colors("k", "number of colors", main_parameters);
  Parameter<int> solution_method("method", "Solution method (none for tester, 0 HC, 1 SD, 2 SA, 3 TS)", main_parameters);
  /** Debugging options **/
  Parameter<unsigned int> verbosity_level("verbosity", "Verbosity level", main_parameters, 0);
  Parameter<unsigned int> plot_level("plot", "Plot level", main_parameters, 0);
  /** Random seed **/
  Parameter<unsigned long int> random_seed("seed", "Random seed", main_parameters);
  Parameter<float> alpha("alpha", "alpha parameter for tabu list", main_parameters);
  Parameter<std::string> model("model", "type of model (auto | direct)", main_parameters);

  Graph g;
  ColoringManager sm(g);
  ColorClashes cc(g);
  sm.AddCostComponent(cc);
  RecolorExplorer ne(g, sm);
  DeltaColorClashes dcc(g, cc);
  ne.AddDeltaCostComponent(dcc);
  //ne.AddCostComponent(cc);
  NodeColoringManager om(g, sm);

  //ProhibitedColorsManager tlm(alpha);
  ClassicalTabuListManager tlm;
  TabuSearch<Graph, Coloring, Recolor, int> ts(g, sm, ne, tlm, "TS");
  HillClimbing<Graph, Coloring, Recolor, int> hc(g, sm, ne, "HC");
  SteepestDescent<Graph, Coloring, Recolor, int> sd(g, sm, ne, "SD");
  SimulatedAnnealing<Graph, Coloring, Recolor, int> sa(g, sm, ne, "SA");

  SimpleLocalSearch<Graph, NodeColoring, Coloring, int> ss(g, sm, om, "SimpleSearch");
  // parse all command line parameters, including those posted by runners and solvers

  if (!CommandLineParameters::Parse(argc, argv, true))
    return 1;

  g.Load(input_file, number_of_colors);

  if (random_seed.IsSet())
    Random::Seed(random_seed);

  RunnerObserver<Graph, Coloring, Recolor, int> ro(verbosity_level, plot_level);

  if (plot_level.IsSet() && verbosity_level.IsSet())
  {
    hc.AttachObserver(ro);
    sd.AttachObserver(ro);
    ts.AttachObserver(ro);
    sa.AttachObserver(ro);
  }

//  std::cout << "Setting up: " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start).count() / 1000000.0 << std::endl;

  auto setting_up = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() / 1000.0;

  if (!solution_method.IsSet())
  {
    // tester
    // FIXME: now the tester should be defined only when it is used (because of State management)
    Tester<Graph, NodeColoring, Coloring, int> tester(g, sm, om);
    // testers
    MoveTester<Graph, NodeColoring, Coloring, Recolor, int> recolor_test(g, sm, om, ne, "Recolor move", tester);
    tester.RunMainMenu();
  }
  else if (solution_method < 4)
  {
    auto seed = Random::seed;
    hc.SetParameter("max_idle_iterations", 1000000UL);
    sa.SetParameter("start_temperature", 5.0);
    MoveRunner<Graph, Coloring, Recolor, int>* runners[4] = { &hc, &sd, &sa, &ts };
    ss.SetRunner(*runners[solution_method]);

    std::tuple<NodeColoring, int, int, double> res = ss.Solve();

    // std::cout << std::get<0>(res) << std::endl;
    // std::cout << "Violations: " << std::get<1>(res) << std::endl;
    // std::cout << "Objectives: " << std::get<2>(res) << std::endl;
    // std::cout << "Time: " << std::get<3>(res) << " s" << std::endl;
    // std::cout << "Iterations: " << runners[solution_method]->Iteration() << std::endl;

    std::cout << std::get<1>(res) << ", " << std::get<3>(res) << ", " << runners[solution_method]->Iteration() << ", " << seed << ", " << g.Vertices() << ", " <<  g.Edges() << ", " << g.k << ", " << setting_up << std::endl;
  }
  return 0;
}

int run_auto_modeling(int argc, const char* argv[])
{
  auto start = std::chrono::system_clock::now();
  ParameterBox main_parameters("main", "Main Program options");

  /** Input parameters **/
  Parameter<std::string> input_file("i", "input file", main_parameters);
  Parameter<unsigned int> number_of_colors("k", "number of colors", main_parameters);
  Parameter<int> solution_method("method", "Solution method (none for tester, 0 HC, 1 SD, 2 SA, 3 TS)", main_parameters);
  /** Debugging options **/
  Parameter<unsigned int> verbosity_level("verbosity", "Verbosity level", main_parameters, 0);
  Parameter<unsigned int> plot_level("plot", "Plot level", main_parameters, 0);
  /** Random seed **/
  Parameter<unsigned long int> random_seed("seed", "Random seed", main_parameters);
  Parameter<float> alpha("alpha", "alpha parameter for tabu list", main_parameters);
  Parameter<std::string> model("model", "type of model (auto | direct)", main_parameters);

  Graph g;
  AutoColoringManager sm(g);
  AutoColorClashes cc(g);
  sm.AddCostComponent(cc);
  AutoRecolorExplorer ne(g, sm);
  AutoDeltaColorClashes dcc(g, cc);
  ne.AddDeltaCostComponent(dcc);
  AutoNodeColoringManager om(g, sm);

//  ProhibitedColorsManager tlm(alpha);
  ClassicalTabuListManager tlm;
  //TabuSearch<Graph, AutoColoring, Recolor, int> ts(g, sm, ne, tlm, "TS");
  HillClimbing<Graph, AutoColoring, Recolor, int> hc(g, sm, ne, "HC");
  SteepestDescent<Graph, AutoColoring, Recolor, int> sd(g, sm, ne, "SD");
  SimulatedAnnealing<Graph, AutoColoring, Recolor, int> sa(g, sm, ne, "SA");

  SimpleLocalSearch<Graph, NodeColoring, AutoColoring, int> ss(g, sm, om, "SimpleSearch");
  // parse all command line parameters, including those posted by runners and solvers

  if (!CommandLineParameters::Parse(argc, argv, true))
    return 1;

  g.Load(input_file, number_of_colors);

  if (random_seed.IsSet())
    Random::Seed(random_seed);

  RunnerObserver<Graph, AutoColoring, Recolor, int> ro(verbosity_level, plot_level);
  if (plot_level.IsSet() && verbosity_level.IsSet())
  {
    hc.AttachObserver(ro);
    sd.AttachObserver(ro);
    //    ts.AttachObserver(ro);
    sa.AttachObserver(ro);
  }

  std::cout << "Setting up: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() / 1000.0 << std::endl;  

  if (!solution_method.IsSet())
  {
    // tester
    // FIXME: now the tester should be defined only when it is used (because of State management)
    Tester<Graph, NodeColoring, AutoColoring, int> tester(g, sm, om);
    // testers
    MoveTester<Graph, NodeColoring, AutoColoring, Recolor, int> recolor_test(g, sm, om, ne, "Recolor move", tester);
    tester.RunMainMenu();
  }
  else if (solution_method < 3)
  {
    MoveRunner<Graph, AutoColoring, Recolor, int>* runners[3] = { &hc, &sd, &sa };
    ss.SetRunner(*runners[solution_method]);

    std::tuple<NodeColoring, int, int, double> res = ss.Solve();

    std::cout << std::get<0>(res) << std::endl;
    std::cout << "Violations: " << std::get<1>(res) << std::endl;
    std::cout << "Objectives: " << std::get<2>(res) << std::endl;
    std::cout << "Time: " << std::get<3>(res) << " s" << std::endl;
  }
  return 0;
}
