#ifndef _NODECOLORINGMANAGER_HH
#define _NODECOLORINGMANAGER_HH

#include "Graph.hh"
#include "NodeColoring.hh"
#include "Coloring.hh"

#include <helpers/StateManager.hh>
#include <helpers/OutputManager.hh>

class NodeColoringManager
: public OutputManager<Graph,NodeColoring,Coloring,int>
{
public:
  NodeColoringManager(const Graph& in, StateManager<Graph,Coloring,int>& sm)
  : OutputManager<Graph,NodeColoring,Coloring,int>(in,"NodeColoringManager")
{}
  void InputState(Coloring&,const NodeColoring&) const;
  void OutputState(const Coloring&,NodeColoring&) const;
};

class AutoNodeColoringManager
: public OutputManager<Graph,NodeColoring,AutoColoring,int>
{
public:
  AutoNodeColoringManager(const Graph& in, StateManager<Graph,AutoColoring,int>& sm)
  : OutputManager<Graph,NodeColoring,AutoColoring,int>(in,"AutoNodeColoringManager")
  {}
  void InputState(AutoColoring&,const NodeColoring&) const;
  void OutputState(const AutoColoring&,NodeColoring&) const;
};

#endif

