#include "ProhibitedColorsManager.hh"

ClassicalTabuListManager::ClassicalTabuListManager(int min, int max, std::string name)
: TabuListManager<Coloring,Recolor,int>(min, max, name)
{}

// the inverse move definition
bool ClassicalTabuListManager::Inverse(const Recolor& rc1, const Recolor& rc2) const
{
  return (rc1.v == rc2.v && rc1.nc == rc2.oc);
}

ProhibitedColorsManager::ProhibitedColorsManager(const Parameter<float>& alpha, int min, int max)
: ClassicalTabuListManager(min, max, "ProhibitedColorsManager"), alpha(alpha)
{}

void ProhibitedColorsManager::InsertIntoList(const Coloring& coloring, const Recolor& rc)
{
	unsigned int tenure = (unsigned int)Random::Int(min_tenure, max_tenure) + (unsigned int)(alpha * coloring.ConflictsSize());
	TabuListItem<Coloring,Recolor,int> li(rc, iter + tenure);
	tlist.push_front(li);
	
	UpdateIteration();
}
