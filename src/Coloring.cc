#include "Coloring.hh"

#include "utils/Random.hh"

Coloring::Coloring(const Graph& in)
: NodeColoring(in), adjacent_conflicts(in.Vertices(), 0)
{
  auto start = std::chrono::system_clock::now();
  std::cout << "State creation: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() / 1000.0 << std::endl;
}

void Coloring::UpdateRedundantData() 
{
  for (Vertex v = 0; v < g.Vertices(); v++)
    for (Vertex w = v + 1; w < g.Vertices(); w++)
    {
      Color c_v = (*this)[v], c_w = (*this)[w];
      if (g.Edge(v, w) && c_v == c_w)
      {
        adjacent_conflicts[v]++;
        adjacent_conflicts[w]++;
        conflicting_nodes.insert(v);
        conflicting_nodes.insert(w);
      }
    }
}

AutoColoring::AutoColoring(const Graph& in) : AutoState<int>(1), in(&in), adjacent_conflicts(in.Vertices())
{
  auto start = std::chrono::system_clock::now();

  color = make_array("color", in.Vertices());
  Exp<int> cost_ex = 0;
  for (int i = 0; i < in.Vertices() - 1; i++)
  {
    for (int j = i + 1; j < in.Vertices(); j++)
      if (in.Edge(i, j))
        cost_ex += (color[i] == color[j]);
  }

  for (int i = 0; i < in.Vertices(); i++)
  {
    Exp<int> adj_ex = 0;
    for (int j = 0; j < in.Vertices(); j++)
      if (in.Edge(i, j))
        adj_ex += (color[i] == color[j]);
    adjacent_conflicts[i] = compile(adj_ex);
  }

  cost = compile(cost_ex);
  std::cout << "AutoState creation: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() / 1000.0 << std::endl;
};

void AutoColoring::UpdateRedundantData()
{
  for (Vertex v = 0; v < in->Vertices(); v++)
    for (Vertex w = v + 1; w < in->Vertices(); w++)
    {
      if (in->Edge(v, w) && value_of(color[v]) == value_of(color[w]))
      {
        conflicting_nodes.insert(v);
        conflicting_nodes.insert(w);
      }
    }
}

bool operator==(const AutoColoring& st1, const AutoColoring& st2)
{
  for (Vertex v = 0; v < st1.color.size(); v++)
  {
    if (st1.value_of(st1.color[v]) == st2.value_of(st2.color[v]))
      return false;
  }
  return true;
}

