#include "Recolor.hh"

std::ostream& operator<<(std::ostream& os, const Recolor& rc)
{
  os << rc.v << " : " << rc.oc << " -> " << rc.nc << " ";
  return os;
}


// input operator
std::istream& operator>>(std::istream& is, Recolor& rc)
{
  std::string dummy;
  is >> rc.v >> dummy >> rc.oc >> dummy >> rc.nc;
  return is; // eventually to fill in
}

// equal operator
bool Recolor::operator==(const Recolor& rc) const
{
  return this->v == rc.v && this->nc == rc.nc;
}

bool Recolor::operator<(const Recolor& rc) const
{
  return this->v < rc.v;
}
