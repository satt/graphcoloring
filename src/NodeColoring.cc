#include "NodeColoring.hh"

std::ostream& operator<<(std::ostream& os, const NodeColoring& out_coloring)
{
  std::set<Vertex> conflicts;
  
  int viol = 0;
  
  // write the coloring
  for (Vertex v = 0; v < out_coloring.g.Vertices(); v++)
  {
    os << out_coloring[v] << " ";
    for (Vertex w = v + 1; w < out_coloring.g.Vertices(); w++)
      if (out_coloring.g.Edge(v,w) && out_coloring[v] == out_coloring[w])
      {
        conflicts.insert(v);
        conflicts.insert(w);
        viol++;
      }
  }
    
    os << std::endl << "Conflicting edges: " << viol << ", Conflicting nodes: " << conflicts.size();
    return os;
}

std::istream& operator>>(std::istream& is, NodeColoring& out_coloring)
{
  for (Vertex v = 0; v < out_coloring.g.Vertices(); v++)
    is >> out_coloring[v];
  return is;
}
