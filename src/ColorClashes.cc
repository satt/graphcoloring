#include "ColorClashes.hh"

int ColorClashes::ComputeCost(const Coloring& coloring) const
{
  const Graph &g = in; // g is an alias for the input Graph
  int conflicts = 0;
  
  for (Vertex v = 0; v < g.Vertices(); v++)
    for (Vertex w = v + 1; w < g.Vertices(); w++)
      if (g.Edge(v, w) && coloring[v] == coloring[w])
        conflicts++;
  return conflicts;
}

int AutoColorClashes::ComputeCost(const AutoColoring& coloring) const
{
  return coloring.CostValue();
}

