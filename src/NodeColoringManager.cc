#include "NodeColoringManager.hh"

void NodeColoringManager::InputState(Coloring& coloring, const NodeColoring& out_coloring) const
{
  const Graph &g = in;
  
  for (Vertex v = 0; v < g.Vertices(); v++)
    coloring[v] = out_coloring[v];
  // update state data
  coloring.UpdateRedundantData();
}

void NodeColoringManager::OutputState(const Coloring& coloring, NodeColoring& out_coloring) const
{
  const Graph &g = in;
  
  for (Vertex v = 0; v < g.Vertices(); v++)
    out_coloring[v] = coloring[v];
}

void AutoNodeColoringManager::InputState(AutoColoring& coloring, const NodeColoring& out_coloring) const
{
  const Graph &g = in;

  for (Vertex v = 0; v < g.Vertices(); v++)
    coloring.set(coloring.color[v], out_coloring[v]);
  coloring.evaluate();
}

void AutoNodeColoringManager::OutputState(const AutoColoring& coloring, NodeColoring& out_coloring) const
{
  const Graph &g = in;

  for (Vertex v = 0; v < g.Vertices(); v++)
    out_coloring[v] = coloring[v];
}